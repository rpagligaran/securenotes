import React, { useState } from 'react';
import { View, Text, Button, TextInput, Alert } from 'react-native';
import TouchID from 'react-native-touch-id';
import { encrypt, decrypt } from './encryption'; // Import your encryption functions here

const App = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [pin, setPin] = useState('');
  const [note, setNote] = useState('');
  const [encryptedNote, setEncryptedNote] = useState('');

  const authenticateWithBiometrics = async () => {
    const promptConfig = {
      title: 'Please Authenticate',
      imageColor: '#000',
      imageErrorColor: '#ff0000',
      sensorDescription: 'Slightly Touch sensor',
      sensorErrorDescription: 'Failed',
      cancelText: 'Cancel',
      fallbackLabel: 'Show Passcode',
      unifiedErrors: false,
      passcodeFallback: true,
    };

    try {
      const auth = await TouchID.authenticate('', promptConfig);
      console.log(auth, "auth");
      setAuthenticated(true);
    } catch (error) {
      Alert.alert('Authentication Failed', error.message);
    }
  };

  const saveNote = () => {
    if (authenticated) {
      log
      // Encrypt the note before saving
      const encrypted = encrypt(note, pin); // Use your encryption method
      setEncryptedNote(encrypted);
      setNote('');
    } else {
      Alert.alert('Authentication Required', 'Please authenticate to save notes.');
    }
  };

  const decryptNote = () => {
    if (authenticated) {
      // Decrypt the note before displaying
      const decrypted = decrypt(encryptedNote, pin); // Use your decryption method
      setNote(decrypted);
    } else {
      Alert.alert('Authentication Required', 'Please authenticate to decrypt notes.');
    }
  };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {authenticated ? (
        <View>
          <Text>Your Secure Notes:</Text>
          <TextInput
            placeholder="Enter your note"
            value={note}
            onChangeText={(text) => setNote(text)}
          />
          <Button title="Save Note" onPress={saveNote} />
          <Button title="Decrypt Note" onPress={decryptNote} />
          <Text>Encrypted Note: {encryptedNote}</Text>
        </View>
      ) : (
        <View>
          <Text>Biometric Authentication</Text>
          <Button title="Authenticate" onPress={authenticateWithBiometrics} />
          <Text>Or</Text>
          <TextInput
            placeholder="Enter PIN"
            secureTextEntry
            value={pin}
            onChangeText={(text) => setPin(text)}
          />
          <Button title="Authenticate with PIN" onPress={() => setAuthenticated(pin === '1234')} />
        </View>
      )}
    </View>
  );
};

export default App;
