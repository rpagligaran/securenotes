import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import AddEditNote from '../src/screens/notes/components/AddEditNote';
import { decrypt, encrypt } from '../encryption';
import noteStore from '../src/store/noteStore';

jest.mock('@react-navigation/native', () => ({
    ...jest.requireActual('@react-navigation/native'),
    useRoute: () => ({
        params: {
            note: {
                id: 1,
                title: 'Test Title',
                description: 'Test Description',
            },
            authenticated: true,
        },
    }),
}));

// const mockUpdateNote = jest.fn();
// const mockAddNote = jest.fn();
// const mockNotes = {
//     addNote: mockAddNote,
//     updateNote: mockUpdateNote,
// };

jest.mock('../src/store/noteStore', () => ({
    __esModule: true,
    default: jest.fn(() => ({
        updateNote: jest.fn(), // Mock the updateNote function
        addNote: jest.fn(),
    })),
}));
import * as encryptionModule from '../encryption';

jest.mock('../encryption', () => {
    return {
        encrypt: jest.fn(),
        decrypt: jest.fn()
        // Add any other mock methods or functions used in your component
    };
});

describe('AddEditNote Component', () => {
    // Test rendering of the component
    it('should render the component', () => {
        const { getByPlaceholderText, getByTestId } = render(<AddEditNote />);
        // Replace with your own test expectations
        expect(getByTestId('add-edit-input-title')).toBeTruthy();
        expect(getByPlaceholderText('Enter your note here...')).toBeTruthy();
    });

    // Test user interaction (simulate user input)
    it('should update the title input', () => {
        const { getByPlaceholderText } = render(<AddEditNote />);
        const titleInput = getByPlaceholderText('Input title here');

        fireEvent.changeText(titleInput, 'New Title');

        // Replace with your own test expectations
        expect(titleInput.props.value).toBe('New Title');
    });

    it('should update the description input', () => {
        const { getByPlaceholderText } = render(<AddEditNote />);
        const descriptionInput = getByPlaceholderText('Enter your note here...');

        fireEvent.changeText(descriptionInput, 'New Description');

        // Replace with your own test expectations
        expect(descriptionInput.props.value).toBe('New Description');
    });

    it('should handle saving', () => {
        // Mock the encryption function to simulate encryption
        const mockEncrypt = jest.spyOn(encryptionModule, 'encrypt');
        mockEncrypt.mockImplementation((data) => `encrypted_${data}`);
      
        // Render the AddEditNote component
        const { getByTestId } = render(<AddEditNote navigation={{ goBack: jest.fn() }} />);
      
        // Simulate user input
        const titleInput = getByTestId('add-edit-input-title');
        const descriptionInput = getByTestId('add-edit-input-description');
        fireEvent.changeText(titleInput, 'New Title');
        fireEvent.changeText(descriptionInput, 'New Description');
      
        // Simulate saving the note
        const saveButton = getByTestId('add-edit-save-button');
        fireEvent.press(saveButton);
      
        // Ensure that mockEncrypt is being called
        // expect(mockEncrypt).toHaveBeenCalledTimes(2); // Adjust this line if necessary
        // expect(mockEncrypt).toHaveBeenCalledWith('New Title'); // Check if encryption was called with the title
        // expect(mockEncrypt).toHaveBeenCalledWith('New Description'); // Check if encryption was called with the description
      });
});
