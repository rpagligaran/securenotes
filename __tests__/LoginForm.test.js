import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import LoginForm from '../src/screens/login/components/LoginForm';

describe('LoginForm', () => {
  it('should render correctly', () => {
    const { getByText, getByPlaceholderText } = render(<LoginForm />);
    
    // Ensure that the title text is rendered.
    expect(getByText('SECURE NOTES')).toBeTruthy();

    // Ensure that the input field is rendered.
    expect(getByPlaceholderText('Enter Password: 1234')).toBeTruthy();

    // Ensure that the "Authenticate" button is rendered.
    expect(getByText('Authenticate')).toBeTruthy();

    // Ensure that the "Biometric Login" text is rendered.
    expect(getByText('Biometric Login')).toBeTruthy();
  });

  it('should call loginUsingPassword when "Authenticate" button is pressed', () => {
    const loginUsingPasswordMock = jest.fn();
    const { getByText } = render(<LoginForm loginUsingPassword={loginUsingPasswordMock} />);
    
    // Find the "Authenticate" button and simulate a press.
    fireEvent.press(getByText('Authenticate'));

    // Verify that the loginUsingPassword function was called.
    expect(loginUsingPasswordMock).toHaveBeenCalled();
  });

  // Add more test cases as needed for your specific use cases.
});
