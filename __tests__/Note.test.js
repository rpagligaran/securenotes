import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import Notes from '../src/screens/Notes';
import noteStore from '../src/store/noteStore';
import * as biometrics from '../src/helpers/biometrics';

// Mock noteStore and biometrics
jest.mock('../src/store/noteStore', () => ({
    __esModule: true,
    default: jest.fn(),
}));

jest.mock('../src/helpers/biometrics', () => ({
    handleBiometricAuthentication: jest.fn(),
}));

const mockNavigate = jest.fn();
const mockNavigation = {
    navigate: mockNavigate,
};

describe('Notes Component', () => {
    it('should render correctly', () => {
        // Mock the notes data
        noteStore.mockReturnValue({
            notes: [{ id: 1, title: 'Note 1', description: "This is note 1" }, { id: 2, title: 'Note 2', description: "This is note 2" }],
        });

        const { getByText, getByPlaceholderText } = render(<Notes navigation={mockNavigation} />);

        // Ensure that the component renders the notes
        expect(getByText('Note 1')).toBeTruthy();
        expect(getByText('Note 2')).toBeTruthy();

        // Ensure that the "New Note" button is present
        expect(getByText('Add New Note')).toBeTruthy();

        // Ensure that the password input is not present initially
        expect(() => getByPlaceholderText('Password')).toThrowError();
    });

    it('should navigate to AddEditNote when note is authenticated', () => {
        // Mock the notes data
        noteStore.mockReturnValue({
            notes: [{ id: 1, title: 'Note 1', descriptions: 'This is note 1' }],
        });
    
        // Mock handleBiometricAuthentication to call the callback with true
        biometrics.handleBiometricAuthentication.mockImplementation(callback => {
            callback(true);
        });
    
        const { getByText } = render(<Notes navigation={mockNavigation} />);
    
        // Simulate clicking on a note to trigger verifyUser
        fireEvent.press(getByText('Note 1'));
    
        // Ensure that the navigation function was called with the correct arguments
        expect(mockNavigation.navigate).toHaveBeenCalledWith('AddEditNote', { note: { id: 1, title: 'Note 1', descriptions: "This is note 1" }, authenticated: true });
    });
});
