import CryptoJS from 'react-native-crypto-js';

// Replace 'your-secret-key' with your actual secret key.
const SECRET_KEY = 'your-secret-key';

// Encrypt a plaintext string
export const encrypt = (text, pin) => {
  try {
    const encryptedText = CryptoJS.AES.encrypt(text, pin || SECRET_KEY).toString();
    return encryptedText;
  } catch (error) {
    console.error('Encryption error:', error);
    return '';
  }
};

// Decrypt an encrypted string
export const decrypt = (encryptedText, pin) => {
  try {
    const bytes = CryptoJS.AES.decrypt(encryptedText, pin || SECRET_KEY);
    const decryptedText = bytes.toString(CryptoJS.enc.Utf8);
    return decryptedText;
  } catch (error) {
    console.error('Decryption error:', error);
    return '';
  }
};
