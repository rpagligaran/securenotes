import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics';

const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: true })

export const isSensorAvailable = async (callback) => {
  try {
    const { biometryType } = await rnBiometrics.isSensorAvailable();

    rnBiometrics.createKeys()
      .then((resultObject) => {
        const { publicKey } = resultObject
        console.log(publicKey, "key key")
        // sendPublicKeyToServer(publicKey)
      })

    // if (biometryType === BiometryTypes.TouchID) {
    //   console.log('TouchID is supported');
    // } else if (biometryType === BiometryTypes.FaceID) {
    //   console.log('FaceID is supported');
    // } else if (biometryType === BiometryTypes.Biometrics) {
    //   console.log('Biometrics is supported');
    // } else {
    //   console.log('Biometrics not supported');
    // }

    callback(true);      
  } catch (error) {
    console.error('Error:', error);
    callback(false)
  }
};

export const handleBiometricAuthentication = async (callback) => {
  try {
    const { success, signature } = await rnBiometrics.createSignature({
      promptMessage: 'Authenticate to continue',
      payload: 'Your authentication payload here',
      cancelButtonText: 'INPUT PASSWORD'
    });

    if (success) {
      // Authentication successful, you can proceed with the action
      console.log('Biometric authentication succeeded');
      callback(true)

    } else {
      // Authentication failed or canceled
      console.log('Biometric authentication failed');
      callback(false)
    }
  } catch (error) {
    if(error?.code?.toString()?.includes("Error creating signature: android.security.KeyStoreException")){
      callback(true);
    } else {
      callback(false);
    }
  }
};
