import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import RootNavigator from './navigation';
import { navigationRef } from './navigation/navigationRef';

const App = () => {
    return (
        <NavigationContainer ref={navigationRef}>
            <RootNavigator />
        </NavigationContainer>
    )
}

export default App