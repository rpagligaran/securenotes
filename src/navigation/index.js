import { View, Text } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/login';
import authStore from '../store/authStore';
import Notes from '../screens/notes';
import AddEditNote from '../screens/notes/components/AddEditNote';

const Stack = createStackNavigator();

const RootNavigator = () => {

    const auth = authStore(state => state);

    console.log(auth, "auth");
    return (
        <Stack.Navigator sceneAnimationEnabled={true}>
            <Stack.Screen
                name="Main"
                options={{
                    headerShown: false,
                }}
                component={auth.authenticated ? Notes : Login}
            />
            <Stack.Screen
                name="AddEditNote"
                options={{
                    headerShown: false,
                }}
                component={AddEditNote}
            />
        </Stack.Navigator>
    )
}

export default RootNavigator