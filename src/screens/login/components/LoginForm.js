import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

const LoginForm = (props) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      padding: 10,
    },
    formContainer: {
      backgroundColor: 'white',
      justifyContent: 'space-around',
      padding: 20,
      paddingVertical: 30,
      borderRadius: 10,
      height: 300,
      elevation: 5,
      borderColor: 'gray',
    },
    titleText: {
      textAlign: 'center',
      color: 'brown',
      fontSize: 20,
      fontWeight: 'bold',
      letterSpacing: 5,
    },
    inputContainer: {
      marginVertical: 20,
      backgroundColor: 'white',
      justifyContent: 'center',
      padding: 5,
      borderRadius: 10,
      elevation: 5,
    },
    authenticateButton: {
      backgroundColor: 'orange',
      borderRadius: 10,
      paddingVertical: 10,
    },
    authenticateButtonText: {
      textAlign: 'center',
      color: 'white',
      fontSize: 18,
    },
    biometricLoginText: {
      textAlign: 'center',
      color: 'orange',
      fontSize: 18,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.titleText}>SECURE NOTES</Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Enter Password: 1234"
            secureTextEntry={true}
            onChangeText={text => props.setPassword(text)}
          />
        </View>
        <TouchableOpacity onPress={() => props.loginUsingPassword()} style={styles.authenticateButton}>
          <View>
            <Text style={styles.authenticateButtonText}>Authenticate</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props.biometricLogin()}>
          <Text style={styles.biometricLoginText}>Biometric Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginForm;
