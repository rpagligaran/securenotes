import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import LoginForm from './components/LoginForm';
import { handleBiometricAuthentication, isSensorAvailable } from '../../helpers/biometrics';
import authStore from '../../store/authStore';

const Login = () => {
  const auth = authStore(state => state);
  const [isAuthenticated, setIsAuthenticated] = useState(auth.authenticated);
  const [password, setPassword] = useState();

  const authenticateWithBiometrics = () => {
    isSensorAvailable(isAvailable => {
      if (isAvailable) {
        handleBiometricAuthentication(authenticated => {
          setIsAuthenticated(authenticated);
          auth.authenticate(authenticated);
        });
      }
    });
  };

  const loginUsingPassword = () => {
    if (password === '1234') {
      auth.authenticate(true);
    } else {
      // Handle invalid password
    }
  };

  useEffect(() => {
    // Call the biometric authentication function here if needed.
  }, []);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      resizeMode: 'cover',
    },
    formContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    authenticateButton: {
      backgroundColor: 'brown',
      paddingVertical: 15,
    },
    authenticateButtonText: {
      textAlign: 'center',
      color: 'white',
      fontSize: 15,
    },
  });

  return (
    <ImageBackground
      source={require('../../../assets/images/sec.png')}
      style={styles.container}
    >
      <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <TouchableOpacity onPress={authenticateWithBiometrics} style={styles.authenticateButton}>
            <View>
              <Text style={styles.authenticateButtonText}>Authenticate</Text>
            </View>
          </TouchableOpacity>
        </View>
      {/* {isAuthenticated === false ? (
        <View style={styles.formContainer}>
          <LoginForm
            biometricLogin={authenticateWithBiometrics}
            loginUsingPassword={loginUsingPassword}
            setPassword={setPassword}
          />
        </View>
      ) : (
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <TouchableOpacity onPress={authenticateWithBiometrics} style={styles.authenticateButton}>
            <View>
              <Text style={styles.authenticateButtonText}>Authenticate</Text>
            </View>
          </TouchableOpacity>
        </View>
      )} */}
    </ImageBackground>
  );
};

export default Login;
