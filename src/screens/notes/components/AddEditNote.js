import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Platform,
} from 'react-native';
import { useRoute } from '@react-navigation/native';
import noteStore from '../../../store/noteStore';
import { decrypt } from '../../../../encryption';

const AddEditNote = (props) => {
  const route = useRoute();
  const note = route?.params?.note;
  const authenticated = route?.params?.authenticated;
  const notes = noteStore(state => state);

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    if (note) { 
      if (authenticated) {
        setTitle(decrypt(note?.title, '1234'));
        setDescription(decrypt(note?.description, '1234'));
      } else {
        setTitle(note?.title);
        setDescription(note?.description);
      }
    }
  }, []);

  const handleCancelPress = () => {
    props.navigation.goBack();
  };

  const handleSavePress = () => {
    console.log(title, description, 'title, description');
    if (note?.id) {
      notes.updateNote({ id: note.id, title, description });
    } else {
      notes.addNote({ id: new Date().getTime(), title, description });
    }
    props.navigation.goBack();
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 10,
      paddingVertical: Platform.OS === 'android' ? 10 : 35,
    },
    contentContainer: {
      flex: 1,
      backgroundColor: 'white',
      borderRadius: 10,
      elevation: 10,
      padding: 10,
      justifyContent: 'space-around',
    },
    titleInput: {
      color: 'brown',
      fontSize: 20,
      fontWeight: 'bold',
      letterSpacing: 2,
    },
    descriptionInput: {
      maxHeight: '80%',
      backgroundColor: '#d6d2d0',
      borderRadius: 10,
      padding: 10,
    },
    buttonContainer: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
    },
    cancelButton: {
      width: '49%',
      backgroundColor: 'gray',
      borderRadius: 10,
      paddingVertical: 10,
    },
    saveButton: {
      width: '49%',
      backgroundColor: 'orange',
      borderRadius: 10,
      paddingVertical: 10,
    },
    buttonText: {
      textAlign: 'center',
      color: 'white',
      fontSize: 15,
    },
  });

  return (
    <ImageBackground
      source={require('../../../../assets/images/sec.png')}
      style={{
        flex: 1,
        resizeMode: 'cover',
      }}
    >
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <TextInput
            value={title}
            onChangeText={text => setTitle(text)}
            style={styles.titleInput}
            placeholder="Input title here"
            testID='add-edit-input-title'
          />
          <ScrollView showsVerticalScrollIndicator={false} style={styles.descriptionInput}>
            <TextInput
              multiline={true}
              numberOfLines={4}
              onChangeText={text => setDescription(text)}
              value={description}
              placeholder="Enter your note here..."
              testID='add-edit-input-description'
            />
          </ScrollView>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={handleCancelPress} style={styles.cancelButton}>
              <View>
                <Text style={styles.buttonText}>Cancel</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity testID='add-edit-save-button' onPress={handleSavePress} style={styles.saveButton}>
              <View>
                <Text style={styles.buttonText}>Save</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

export default AddEditNote;
