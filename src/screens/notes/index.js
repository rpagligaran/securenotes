import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Platform,
  TextInput,
  StyleSheet,
} from 'react-native';
import noteStore from '../../store/noteStore';
import { handleBiometricAuthentication } from '../../helpers/biometrics';
import { decrypt } from '../../../encryption';

const Notes = ({ navigation }) => {
  const notes = noteStore(state => state);
  const [isViewable, setIsViewable] = useState(false);


  const verifyUser = (item, purpose) => {
    handleBiometricAuthentication(authenticated => {
      if (authenticated) {
        if (purpose === 'View Note') {
          navigation.navigate('AddEditNote', { note: item, authenticated });
        } else {
          setIsViewable(true);
        }
      }
    });
  };

  useEffect(() => {
    setIsViewable(notes.length > 0);
  }, [notes])

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 10,
      paddingVertical: Platform.OS === 'android' ? 10 : 35,
    },
    contentContainer: {
      flex: 1,
      backgroundColor: 'white',
      borderRadius: 10,
      elevation: 10,
      padding: 10,
      justifyContent: 'space-around',
    },
    notesText: {
      color: 'brown',
      fontSize: 20,
      fontWeight: 'bold',
      letterSpacing: 2,
    },
    scrollView: {
      maxHeight: '85%',
      backgroundColor: '#d6d2d0',
      borderRadius: 10,
      padding: 10,
    },
    title: {
      fontSize: 18,
    },
    separator: {
      height: 1,
      backgroundColor: '#7e7d7d',
      width: '100%',
      marginTop: 10
    },
    newNoteButton: {
      backgroundColor: 'orange',
      borderRadius: 10,
      paddingVertical: 10,
    },
    newNoteButtonText: {
      textAlign: 'center',
      color: 'white',
      fontSize: 15,
    },
    imageBackground: {
      flex: 1,
      resizeMode: 'cover',
    },
  });

  return (
    <ImageBackground
      source={require('../../../assets/images/sec.png')}
      style={styles.imageBackground}
    >
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
            <Text style={styles.notesText}>NOTES</Text>
            <TouchableOpacity onPress={() => verifyUser(null, 'View Title')}>
              <Text style={{ color: "orange" }}>View Title</Text>
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}>
            {notes.notes.map((item, index) => (
              <View key={index}>
                <TouchableOpacity onPress={() => verifyUser(item, 'View Note')}>
                  <Text numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
                    {isViewable ? decrypt(item.title, "1234") : item.title}
                  </Text>
                </TouchableOpacity>
                <View style={styles.separator} />
              </View>
            ))}
          </ScrollView>
          <TouchableOpacity onPress={() => navigation.navigate('AddEditNote')}>
            <View style={styles.newNoteButton}>
              <Text style={styles.newNoteButtonText}>Add New Note</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

export default Notes;
