import { create } from 'zustand'

const authStore = create(set => ({
    authenticated: null,
    authenticate: async (isAuthenticated) => {
        set({ authenticated: isAuthenticated })
    }
}));

export default authStore;
