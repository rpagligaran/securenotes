import { create } from 'zustand';
import { encrypt } from '../../encryption';

const initialState = {
  notes: [],
};

const noteStore = create((set, get)=> ({
  ...initialState,  
  addNote: async (note) => {
    note.title = encrypt(note.title, "1234");
    note.description = encrypt(note.description, "1234");
    const notes = [...get().notes, note];
    set({ notes});
  },
  updateNote: async (updated) => {
    updated.title = encrypt(updated.title, "1234");
    updated.description = encrypt(updated.description, "1234");
    const notes = get().notes.map((note) => {
      if (note.id === updated.id) {
        return { ...note, ...updated };
      } else {
        return note;
      }
    });

    set({ notes });
  },
}));

export default noteStore;
